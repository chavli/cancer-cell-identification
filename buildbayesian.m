close all;
clc; clear;

%Load image data and train a bayesian model for classification. 
%   -1: Gleason G3
%   1:  Gleason G4

%attributes of data:
%   1. image id (integer)
%   2. num_cells (integer)
%   3. average cell roundness (float)
%   4. variance cell roundness (float)
%   5-8. covariance of cell centers (float)
%   9. cell area/total area ratio (float)
%   10. texture area (integer)
%   11. variance of subband 1 curvelet coef (float)
%   12. variance of subband 6 curvelet coef (float)
%   13. percentage of overall picture (float)
%   14. class label (categorical {-1. 1})

%these datasets are created by createdatasets.m
raw_training_data = load('gleason_training_data.csv');
raw_test_data = load('gleason_test_data.csv');

%pick out the features we want + include the class label
training_data = raw_training_data(:, [3, 12, 14]);
test_data = raw_test_data(:, [3, 12, 14]);

M = size(training_data, 2);
foo = zeros(10, 6);

c1_idx = find(training_data(:, end) == -1);
c2_idx = find(training_data(:, end) == 1);

%split training data by class
N = size(training_data, 2);
training_c1 = training_data(c1_idx, :);
training_c2 = training_data(c2_idx, :);

granularity = 2^12;

%hold the pdfs for each variable for each class
densities = zeros(2, N-2, granularity);
meshes = zeros(2, N-2, granularity);

%class priors
priors = [  size(training_c1, 1) / size(training_data, 1), ...
            size(training_c2, 1) / size(training_data, 1) ];

%KDE for calculating the PDF's used for the likelihoods
for n=1:(N-1)
    x_c1 = training_c1(:, n);
    x_c2 = training_c2(:, n);
    
   
    [h_c1, density_c1, mesh_c1, cdf_c1] = kde(x_c1, granularity);
    [h_c2, density_c2, mesh_c2, cdf_c2] = kde(x_c2, granularity);
    
    densities(1, n, :) = (density_c1 / sum(density_c1));
    densities(2, n, :) = (density_c2 / sum(density_c2));

    meshes(1, n, :) = mesh_c1';
    meshes(2, n, :) = mesh_c2';
    
    figure;
    hold on;
    plot(mesh_c1, (density_c1 / sum(density_c1))');
    plot(mesh_c2, (density_c2 / sum(density_c2))', 'r');
    scatter(x_c1, zeros(size(x_c1, 1), 1), 'b');
    scatter(x_c2, zeros(size(x_c2, 1), 1), 'xr');
end

%test_data = training_data

results = zeros(size(test_data, 1), 2);

%calculate posterior likelihood for each test sample
for i=1:size(test_data, 1)
    sample = test_data(i, 1:end-2);
    
    %class 1
    post_c1 = 1;
    for x=1:size(sample, 2)
        idx = min(find(meshes(1, x, :) >= sample(x)));
        if(size(idx, 1) == 0)
            idx = granularity;
        end
        post_c1 = post_c1 * densities(1, x, idx);
    end
    post_c1 = post_c1 * priors(1);
    
    %class 2
    post_c2 = 1;
    for x=1:size(sample, 2)
        idx = min(find(meshes(2, x, :) >= sample(x)));
        if(size(idx, 1) == 0)
            idx = granularity;
        end        
        post_c2 = post_c2 * densities(2, x, idx);
    end
    post_c2 = post_c2 * priors(2);
    
    guess = 1;
    if post_c1 >= post_c2
        guess = -1;
    end
    
    output = sprintf('post_c1: %f, post_c2: %f, label: %d, weight: %f',...
        post_c1, post_c2, guess, raw_test_data(i, 10));
    disp(output);
    results(i, :) = [guess, raw_test_data(i, 10)];
    
end

img1 = results(1:16, :);
img2 = results(17:end, :);



%normalize the density
%{
density = density / sum(density);
idx = min(find(mesh >= x));
disp(density(idx));
%}
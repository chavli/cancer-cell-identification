close all;
clc; clear;

%Load image data and train and test a 2 class SVM. 
%   -1: Gleason G3
%   1:  Gleason G4

%attributes of data:
%   1. image id (integer)
%   2. num_cells (integer)
%   3. average cell roundness (float)
%   4. variance cell roundness (float)
%   5-8. covariance of cell centers (float)
%   9. cell area/total area ratio (float)
%   10. texture area (integer)
%   11. variance of subband 1 curvelet coef (float)
%   12. variance of subband 6 curvelet coef (float)
%   13. percentage of overall picture (float)
%   14. class label (categorical {-1. 1})

%these datasets are created by createdatasets.m
raw_training_data = load('gleason_training_data.csv');
raw_test_data = load('gleason_test_data.csv');

%pick out the features we want + include the class label
training_data = raw_training_data(:, [3, 6, 12, 14]);
test_data = raw_test_data(:, [3, 6, 12, 14]);

M = size(training_data, 2);
foo = zeros(10, 6);

%grid search for parameters
for n=.1:.1:.9
    i = uint32(n/.1 + 1);
    foo(i, 1) = n;   
    for g=1:10
        gamma = 1/g;
        foo(1, g+1) = gamma; 
        
        %leave one out cross-validation
        args = sprintf('-s 1 -t 2 -v %d -n %f -g %f -m 1000 -q', ...
            size(training_data, 1), n, gamma);
        
        cv_error = svmtrain(training_data(:,M), training_data(:, 1:M-1), args);
        foo(i, g+1) = cv_error;
    end
end
figure; surf(foo(2:end, 2:end));

%use the best parameters and train and test model
args = sprintf('-s 1 -t 2 -n %f -g %f -m 1000', .5, .5);
model = svmtrain(training_data(:,M), training_data(:, 1:M-1), args);

%test_data = training_data;
%raw_test_data = raw_training_data;
c = 16;

[predictions, accuracy, prob] = svmpredict(test_data(:, M), ...
    test_data(:, 1:M-1), model);

%break apart the predictions into the individual test sets
total_g3g4 = [raw_test_data(1:c, 1), predictions(1:c), ...
    raw_test_data(1:c, 9), raw_test_data(1:c, 13), ...
    raw_test_data(1:c, 9) .* raw_test_data(1:c, 13)];

total_g4g3 = [raw_test_data(c+1:end, 1), predictions(c+1:end), ...
    raw_test_data(c+1:end, 9), raw_test_data(c+1:end, 13) ... 
    raw_test_data(c+1:end, 9) .* raw_test_data(c+1:end, 13)];

%for each test set, calculate what percentage of each class each image
%belongs to.
a = sum(total_g3g4(:, 5));
idx1 = find(total_g3g4(:, 2) == -1);
idx2 = find(total_g3g4(:, 2) == 1);
disp([-1, sum(total_g3g4(idx1, 5))/a, 1, sum(total_g3g4(idx2, 5))/a]);

a = sum(total_g4g3(:, 5));
idx1 = find(total_g4g3(:, 2) == -1);
idx2 = find(total_g4g3(:, 2) == 1);
disp([ -1,  sum(total_g4g3(idx1, 5))/a, 1, sum(total_g4g3(idx2, 5))/a]);


%finally, display the classified images.
test_files = {'07', '14'};
%test_files = {'04', '16'};

%reform the original picture by combining the subimages for each set of
%test files. the final combined image is displayed with color coded
%subimages. 
%   BLUE(-1) = G3
%   GREEN(1) = G4
%
for t=1:size(test_files, 2)
    counter = 0;
    row_img = []; whole_img = [];
    
    if( t == 1 )
        results = total_g3g4;
    else
        results = total_g4g3;
    end
    
    for z=1:16
        filename = sprintf('model_data/1-raw%d-0471-Feu-%s-1.tif', z, test_files{t});
        img_data = imread(filename, 'TIFF');
        idx = find(results(:, 1) == z);
        
        %draw the colored border around each image
        if( ~isempty(idx) )
            if( results(idx, 2) == -1 )
                img_data = imgaddborder(img_data, 3, [0, 0, 1]);
            else
                img_data = imgaddborder(img_data, 3, [0, 1, 0]);
            end
        else
            img_data = imgaddborder(img_data, 3, [1, 1, 1]);
        end
        
        %build the final image row by row. "4" is the number of subimages
        %per row.
        row_img = [row_img, img_data];
        counter = counter + 1; 
        if( mod(counter, 4) == 0 )
            whole_img = [whole_img; row_img];
            row_img = [];
        end
    end
    figure; imshow(whole_img);
    title(test_files{t});
end

addpath('./fdct_wrapping_matlab');
addpath('./fdct_usfft_matlab');
addpath('./ridgelet');
addpath('./libsvm-3.17/matlab');
addpath('/home/cha/Programming/octave-libs/mlcl');

%feature vector of each sample:
%   1. image id (integer)
%   2. num_cells (integer)
%   3. average cell roundness (float)
%   4. variance cell roundness (float)
%   5-8. covariance of cell centers (float)
%   9. cell area/total area ratio (float)
%   10. texture area (integer)
%   11. variance of subband 1 curvelet coef (float)
%   12. variance of subband 6 curvelet coef (float)
%   13. percentage of overall picture (float)
%   14. class label (categorical {-1. 1})

close all;
clc; clear;

M = 1024;  %dimensions of image samples were pulled from
m = 256;   %dimensions of a sample
Z = (M / m)^2;

total_area = m^2;

class = 0;
fileids = {'04', '16', '07', '14'};
total_dataset = [];
markers = zeros(1, 4);

%curvelet stuff
subbands = 6;
all_sigma_data = zeros(subbands, Z, size(fileids, 2));
all_mu_data = zeros(subbands, Z, size(fileids, 2));
mu_data = zeros(subbands, Z);
sigma_data = zeros(subbands, Z);



mark = 0;
for f=1:length(fileids)
    image_dataset = [];
    for i=1:Z
        
        %1. incoming images are raw 256x256 grey-scale. all needed images
        %are created by createimages.m
        rawfile = sprintf('1-raw%d-0471-Feu-%s-1.tif', i, fileids{f});
        rawpath = sprintf('model_data/%s', rawfile);
        raw_img = imread(rawpath, 'TIFF');
        output = sprintf('loading %s...', rawpath);
        disp(output);
        
        
        %2. enhance the contrast
        working_img = adapthisteq(raw_img);
        working_img = reshape(boxcox(double(working_img(:))), m, m);
        working_img = working_img ./ max(working_img(:));
        
        %2a. look for bad pixels
        idx = find(abs(working_img) > 1);
        working_img(idx ) = .5;
        
        %3. we now create a separate image where the background is blurred.
        %the blurred image is used for removing white space.
        blur_img = anisotropic_diffusion(working_img, 4, 2);
        
        %4. k-means is performed on the blurred and enhanced image. the
        %former is used for removing white space and the latter is used to
        %detect cells.
        mu = [0; .25; .5; .75;];
        cluster_dim = 1;
        
        [cell_img, cell_idx] = imgkmeans(working_img, m, cluster_dim, mu);
        [bg_img, bg_idx] = imgkmeans(blur_img, m, cluster_dim, mu);
        
        %5. extract features for describing cells: avg roundness, roundness
        %variance, spread of cells (covariance of centers), # of cells (estimate).
        roundness = zeros(1, 1000);
        centers = [];
        num_cells = 0;
        cell_area = 0;
        
        %k-means value search for cells with
        search_idx = 1; 
        
        while( num_cells == 0 )
        
            t = (cell_idx == search_idx);
            cell_map = reshape(t, m, m);

            [B, L] = bwboundaries( cell_map, 'noholes');
            stats = regionprops(L, 'Area', 'Centroid', 'Eccentricity');

            %5b. go through each discovered "cell"
            for j=1:length(B)
                cell_bounds = B{j};

                %perimeter
                delta = diff(cell_bounds).^2;
                p = sum(sqrt(sum(delta,2)));

                %area
                a = stats(j).Area;

                %roundness
                r = (4*pi*a)/(p^2);
                %r = stats(i).Eccentricity;
                
                %a ratio > 1 is invalid
                if( r > 1 )
                    r = 0;
                end
                
                %requirements for being a cell
                %if((a >= 60 && p > 40) || (a >= 40 && r < .8))
                if( (a >= 60 || r > .90) && p > 30 )
                    num_cells = num_cells + 1;
                    roundness(num_cells) = r;
                    cell_area = cell_area + a;

                    centers = [centers; stats(j).Centroid];
                end
            end
            
            %if no cells were found, try the next k-means cluster
            search_idx = search_idx + 1;
        end
        
        %invert bits the cell map
        cell_map = xor(cell_map, 1);
        
        mu_roundness = mean(roundness(1:num_cells));
        var_roundness = std(roundness(1:num_cells));
        cov_roundness = corr(centers);
        cov_roundness = cov_roundness(:);
        
        %5c. cells are done, now look at white space 
        white_area = 0;
        trivial = true;
        t = (bg_idx == length(mu));
        white_map = reshape(t, m, m);
        [B, L] = bwboundaries( white_map );
        stats = regionprops(L, 'Area');
        for j=1:length(B)
            white_bounds = B{j};
            a = stats(j).Area;
            white_area = white_area + a;
            
            %only count areas as "white"  if there are big blocks of it
            if(a > 9500)
                trivial = false;
            end
        end
        
        raw_img = raw_img .* uint8(cell_map);
        
        %5d. now calculate the ratio of cell area to texture area
        if( ~trivial )
            texture_area = total_area - white_area;
            white_map = xor(white_map, 1);
            raw_img = raw_img .* uint8(white_map);
        else
            texture_area = total_area;
        end
        
        cell_ratio = cell_area / texture_area;
        
        %BEGIN CURVELET STUFF
        C = fdct_wrapping(raw_img, 0, 2, subbands);
        temp_data = [];
        
        %examine each curvelet subband of the image
        L = size(C, 2);
        for l=1:subbands
            a_mean = 0;
            a_std = 0;
            %angles
            A = size(C{l}, 2);
            for a=1:A
                coef = real(C{l}{a}(:));
                a_mean = a_mean + mean(coef);
                a_std = a_std + std(coef);
            end
            a_mean = a_mean / A;
            a_std = a_std / A;
            temp_data = [temp_data; [a_mean, a_std]];
        end
        
  
        sigma_data(:, i) = temp_data(:, 2);
        mu_data(:, i) = temp_data(:, 1);      
        %END CURVELET
        
        %6. create the feature vector representing the image
        sample = [  i, num_cells, mu_roundness, var_roundness, ...
            cov_roundness', cell_ratio, texture_area, ...
            temp_data(1, 2), temp_data(6, 2)];

        disp(sample);
        %ignore samples with NaNs
        if(sum(isnan(sample)) == 0 && sum(isinf(sample)) == 0)
            image_dataset = [image_dataset; sample];
            mark = mark + 1;
        end
        
    end
    
    
    
    %create one more feature, image ratio, which represents how much of the
    %overall image each subimage represents: texture_area(i) /
    %total_texture_area (basically a normalized column 10)
    image_area = sum(image_dataset(:, 10));
    image_ratio = image_dataset(:, 10) / image_area;
    image_dataset = [image_dataset, image_ratio];
    total_dataset = [total_dataset; image_dataset];
    markers(f) = mark;
end

N = size(total_dataset, 1);
M = size(total_dataset, 2);

labels = zeros(N, 1);
labels(1:markers(1), 1) = -1;
labels(markers(1)+1:markers(2), 1) = 1;
labels(markers(2)+1:end, 1) = 0;

%attach labels to dataset. 0 = unknown
total_dataset = [total_dataset, labels];

train_data = total_dataset(1:markers(2), :);
test_data = total_dataset(markers(2)+1:end, :);

filename = 'gleason_training_data.csv';
csvwrite(filename, train_data);

filename = 'gleason_test_data.csv';
csvwrite(filename, test_data);


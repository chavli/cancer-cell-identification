addpath('./ridgelet');
close all;
clc; clear;

class = 1;
filename = '0471-Feu-14-1.tif';
filepath = sprintf('images/%s', filename);
img_data = imread(filepath, 'TIFF');

M = 512;
m = 256;

gs_img_data = rgb2gray(img_data);
img_dim = size(gs_img_data, 1)/2;
gs_img_data = gs_img_data((img_dim-M:img_dim+(M-1)), (img_dim-M:img_dim+(M-1)), :);

height = size(gs_img_data, 1);
width = size(gs_img_data, 2);

%split the whole image into smaller mxm blocks. there will be 16 images
orig_blocks = imgsectors(gs_img_data, m, m, 256);

%begin processing the whole image
working_img = adapthisteq(gs_img_data);
working_img = reshape(boxcox(double(working_img(:))), height, width);
working_img = working_img ./ max(working_img(:));
%working_img = anisotropic_diffusion(working_img, 15, 2);

%image segmentation
%mu = [0; .25; .5; .75];
mu = [0.00000; 0.16667; 0.33333; 0.50000; 0.66667; 0.83333];
working_blocks = imgsectors(working_img, m, m, 1);

%canny threshold
canny_thresh = [.10, .50];


Z = ((2*M) / m)^2;
for i=1:Z
    ip_img = working_blocks(:, :, :, i);
    raw_img = orig_blocks(:, :, :, i);
    [seg_img, labels] = imgkmeans(ip_img, m, 1, mu);
    
    rawfile = sprintf('model_data/%d-raw%d-%s',class, i, filename);
    imwrite(raw_img, rawfile, 'TIFF');     
    
    %{
    if(length(labels(:)) == m^2 )

        %pick out the cells for edge detection
        f = (labels == 1); 
        cellfile = sprintf('model_data/%d-cell%d-%s',class, i, filename);
        imwrite(ip_img, cellfile, 'TIFF');     
        
        edges = edge(ip_img, 'canny', canny_thresh);
        edgefile = sprintf('model_data/%d-edge%d-%s',class, i, filename);
        imwrite(edges, edgefile, 'TIFF');            

        %pick out the texture
        f = (labels ~= 1);
        txur = reshape(f, m, m) .* raw_img;
        txurfile = sprintf('model_data/%d-txur%d-%s',class, i, filename);
        imwrite(txur, txurfile, 'TIFF');

    end
    %}
end    

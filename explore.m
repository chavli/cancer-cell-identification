addpath('./ridgelet');
close all;
clc; clear;


M = 384;   %desired image size 2Mx2M
m = 256;

grid_labels = {'top left', 'top mid', 'top right', 'mid left', ... 
    'mid mid', 'mid right', 'bottom left', 'bottom mid', 'bottom right',};

filename = 'images/0471-Feu-16-1.tif';

img_data = imread(filename, 'TIFF');

%convert to gray-simcale
gs_img_data = rgb2gray(img_data);

img_dim = size(gs_img_data, 1)/2;

%cut out a square area of the image from the middle
midimg = gs_img_data((img_dim-M:img_dim+(M-1)), (img_dim-M:img_dim+(M-1)), :);

orig_grid = imgsectors(midimg, m, m, 256);

%some craziness with image normalization
figure; imshow(midimg); title('original image');
subimg = adapthisteq(midimg);   
%figure; imshow(subimg); title('adapthiseq image');


%normalize the pixel distribution
norm_img = reshape(boxcox(double(subimg(:))), 2*M, 2*M);
norm_img = norm_img ./ max(norm_img(:));
%figure; imshow(norm_img ./ max(norm_img(:))); title('normalized image');

%try to blur background noise
%ansio_img = anisotropic_diffusion(norm_img, 15, 2);
ansio_img = norm_img;
%figure; imshow(ansio_img); title('anisotropic image');

%inv_img = imcomplement(ansio_img);
%figure; imshow(inv_img); title('inverted image');

K = 4;
built_img = imgkmeans(ansio_img, 2*M, 1, K);
%figure; imshow(built_img); title('k-means segmented image');

img_grid = imgsectors(ansio_img, m, m, 1);


%[low thresh, high thresh]
%the high thresh is used to detect where edges start.
%the low thresh is used for tracing edges from where they start
canny_thresh = [.10, .50];
foo = zeros(m, m, 9);

%{
figure; title(filename);
for j=1:9    
    %try silverman's rule of thumb for choosing bandwidth
    cur_img = img_grid(:, :, :, j);
    sigma = std(cur_img(:));
    n = m^2;
    h = 1.06*sigma*(n^(-.2));
    edge_img = edge(cur_img, 'canny', canny_thresh, h);  %find the edges
    
    foo(:, :, j) = edge_img;
    
    subplot(3,6,1 + (2*(j - 1))); imshow(cur_img); title(grid_labels{j});
    subplot(3,6, (2 * j)); imshow(edge_img); title(grid_labels{j});
end

figure;
for j=1:9    
    ridge_img = ridgelet(foo(:, :, j), 2, 0);
    subplot(3, 3, j); plot(ridge_img');
end
%}

%try k-means clustering for image segmentation
%each datapoint (cluster of pixels) 
%pick 1 of the 256x256 images
num_means = 4;
cluster_dim = 1;

for z=6:6
    test_img = img_grid(:, :, :, z);
    %idx = meanshift(test_img(:), 4);
    %idx = mahalkmeans(test_img(:), 4);
    mu = [0; .25; .5; .75];
    %mu = [0; .2; .4; .6; .8;];
    %mu = [0.00000; 0.16667; 0.33333; 0.50000; 0.66667; 0.83333];
    [built_img, idx] = imgkmeans(test_img, m, cluster_dim, mu);
    
    %figure; imshow(built_img);
    figure;
    subplot(2, 2, 1); imshow(orig_grid(:, :, :, z));
    subplot(2, 2, 2); imshow(built_img);
    
    %cell_img = reshape(f, m, m) .* orig_grid(:, :, :, z);
    %subplot(2, 2, 3); imshow( edge(cell_img, 'canny', canny_thresh));
    
    %cell processing
    subplot(2, 2, 3); imshow(orig_grid(:, :, :, z));
    
    %cell processing
    hold on;
    f = (idx == 1); 
    [B, L] = bwboundaries( reshape(f, m, m), 8, 'noholes');
    stats = regionprops(L, 'Area', 'Centroid');
    for i=1:length(B)
        bounds = B{i};

        %perimeter
        delta_sq = diff(bounds).^2;
        p = sum(sqrt(sum(delta_sq,2)));
        
        %area of cell
        a = stats(i).Area;
        roundness = (4*pi*a)/(p^2);
        
        %invalid
        if( roundness > 1 )
            roundness = 0;
        end
        
        if( (a >= 60 || roundness > .90) && p > 30 )
            %output = sprintf('%2.2f, %2.2f, %2.2f', roundness, a, p);
            %disp(output);
            plot(bounds(:, 2), bounds(:,1), 'r'); 
            %a = patch(bounds(:, 2), bounds(:,1), 'b');
        end
    end

    %white space (emptiness)
    subplot(2, 2, 4); imshow(orig_grid(:, :, :, z));
    hold on;
    
    f = (idx == length(mu)); 
    [B, L] = bwboundaries( reshape(f, m, m), 8, 'noholes');
    stats = regionprops(L, 'Area', 'Centroid');
    for i=1:length(B)
        bounds = B{i};

        %perimeter
        %delta_sq = diff(bounds).^2;
        %p = sum(sqrt(sum(delta_sq,2)));
        
        %area of cell
        a = stats(i).Area;
        if(a > 150)
            plot(bounds(:, 2), bounds(:,1), 'g'); 
        end
        
    end    
    
    %texture
    %f = (idx ~= 1); 
    %subplot(2, 2, 4); imshow( reshape(f, m, m) .* orig_grid(:, :, :, z));
   
    %subplot(1, 2, 1); imshow(reshape(idx, 256, 256) ./ 4);
    %subplot(1, 2, 2); imshow(built_img);
end


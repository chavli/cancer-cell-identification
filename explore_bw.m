addpath('./ridgelet');
close all;
clc; clear;


M = 512;   %desired image size 2Mx2M
m = 256;

grid_labels = {'top left', 'top mid', 'top right', 'mid left', ... 
    'mid mid', 'mid right', 'bottom left', 'bottom mid', 'bottom right',};

filename = 'images/0471-Feu-04-1.tif';

img_data = imread(filename, 'TIFF');

%convert to gray-simcale
gs_img_data = rgb2gray(img_data);
img_dim = size(gs_img_data, 1)/2;

%cut out a square area of the image from the middle
midimg = gs_img_data((img_dim-M:img_dim+(M-1)), (img_dim-M:img_dim+(M-1)), :);

orig_grid = imgsectors(midimg, m, m, 256);

%some craziness with image normalization
figure; imshow(midimg); title('original image');
subimg = adapthisteq(midimg);   

%normalize the pixel distribution
norm_img = reshape(boxcox(double(subimg(:))), 2*M, 2*M);
norm_img = norm_img ./ max(norm_img(:));

img_grid = imgsectors(norm_img, m, m, 1);

%try to blur background noise
ansio_img = anisotropic_diffusion(norm_img, 4, 2);
ansio_grid = imgsectors(ansio_img, m, m, 1);

%try k-means clustering for image segmentation
%each datapoint (cluster of pixels) 
cluster_dim = 1;
%mu = [0; .25; .5; .75];
mu = [0; .2; .4; .6; .8;];

Z = ((2*M) / m)^2;

for z=12:12
    test_img = img_grid(:, :, :, z);
    
    [built_img, idx] = imgkmeans(test_img, m, cluster_dim, mu);
    
    %figure; imshow(built_img);
    figure;
    subplot(2, 2, 1); imshow(orig_grid(:, :, :, z));
    subplot(2, 2, 2); imshow(built_img);
    
    %cell processing
    subplot(2, 2, 3); imshow(orig_grid(:, :, :, z));
    
    %cell processing
    hold on;
    f = (idx == 1); 
    [B, L] = bwboundaries( reshape(f, m, m), 8, 'noholes');
    stats = regionprops(L, 'Area', 'Centroid', 'Eccentricity');
    for i=1:length(B)
        bounds = B{i};

        %perimeter
        delta_sq = diff(bounds).^2;
        p = sum(sqrt(sum(delta_sq,2)));
        
        %area of cell
        a = stats(i).Area;
        %roundness = (4*pi*a)/(p^2);
        roundness = stats(i).Eccentricity;
        
        if((a >= 60 && p > 40) || (a >= 40 && roundness < .8))
            disp([roundness, a, p]);
            plot(bounds(:, 2), bounds(:,1), 'r'); 
        end;
        
        %invalid
        if( roundness > 1 )
            roundness = 0;
        end
        
        %{
        if( a >= 50 && (roundness > .99 && a > 120) || (a >= 40 && roundness < .6) || (p > 50 && roundness < .99) )
            %output = sprintf('%2.2f, %2.2f, %2.2f', roundness, a, p);
            %disp(output);
            plot(bounds(:, 2), bounds(:,1), 'b'); 
            %a = patch(bounds(:, 2), bounds(:,1), 'b');
        end
        %}
    end

    %white space (emptiness)
    test_img = ansio_grid(:, :, :, z);
    [built_img, idx] = imgkmeans(test_img, m, cluster_dim, mu);
    
    
    %subplot(2, 2, 4); imshow(built_img);
    subplot(2, 2, 4); imshow(orig_grid(:, :, :, z));
    
    hold on;
    
    max_a = 0;
    s = 0;
    
    f = (idx == length(mu)); 
    [B, L] = bwboundaries( reshape(f, m, m), 8);
    stats = regionprops(L, 'Area', 'Centroid', 'Solidity');
    for i=1:length(B)
        bounds = B{i};

        %perimeter
        %delta_sq = diff(bounds).^2;
        %p = sum(sqrt(sum(delta_sq,2)));
        
        %area of cell
        a = stats(i).Area;
        if(a > 9500 && a > max_a)
            max_a = a;
            s = stats(i).Solidity;
            max_bounds = bounds;
        end
        plot(bounds(:, 2), bounds(:,1), 'b');
    end
    
    if(max_a > 0)
        output = sprintf('%2.2f -- %2.2f', max_a, s);
        disp(output);
        plot(max_bounds(:, 2), max_bounds(:,1), 'g'); 
    end
    %texture
    %f = (idx ~= 1); 
    %subplot(2, 2, 4); imshow( reshape(f, m, m) .* orig_grid(:, :, :, z));
   
    %subplot(1, 2, 1); imshow(reshape(idx, 256, 256) ./ 4);
    %subplot(1, 2, 2); imshow(built_img);
end


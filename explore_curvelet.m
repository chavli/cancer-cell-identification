addpath('./fdct_wrapping_matlab');
addpath('./fdct_usfft_matlab');
addpath('./ridgelet');
addpath('./libsvm-3.17/matlab');
addpath('/home/cha/Programming/octave-libs/mlcl');

close all
%clc; clear;

screen_size = get(0, 'ScreenSize'); 

M = 1024;  %dimensions of image samples were pulled from
m = 256;   %dimensions of a sample
Z = (M / m)^2;

fileids = {'04', '16', '07', '14'};
subbands = 6;

%all the extracted parameters for the GGD
all_sigma_data = zeros(subbands, Z, size(fileids, 2));
all_mu_data = zeros(subbands, Z, size(fileids, 2));
mu_data = zeros(subbands, Z);
sigma_data = zeros(subbands, Z);


for f=1:4
    for i=1:Z
        %1. incoming images are raw 256x256 grey-scale
        rawfile = sprintf('1-raw%d-0471-Feu-%s-1.tif', i, fileids{f});
        rawpath = sprintf('model_data/%s', rawfile);
        raw_img = imread(rawpath, 'TIFF');
        output = sprintf('loading %s...', rawpath);
        disp(output);   
        
        
        %2. enhance the contrast
        working_img = adapthisteq(raw_img);
        working_img = reshape(boxcox(double(working_img(:))), m, m);
        working_img = working_img ./ max(working_img(:));        
        
        %3. create a blurred image
        blur_img = anisotropic_diffusion(working_img, 4, 2);
        
        %4. prepare for k-means
        mu = [0; .25; .5; .75];
        cluster_dim = 1;
        
        [cell_img, cell_idx] = imgkmeans(working_img, m, cluster_dim, mu);
        [bg_img, bg_idx] = imgkmeans(blur_img, m, cluster_dim, mu);        
        
        %k-means value that represents cells
        search_idx = 1;
        num_cells = 0;
        
        %increase cell_idx until cells are found
        while( num_cells == 0 )
            t = (cell_idx == search_idx);
            cell_map = reshape(t, m, m);
            [B, L] = bwboundaries( cell_map, 'noholes');
            num_cells = length(B);
            search_idx = search_idx + 1;
        end
        
        %invert bits the cell map
        cell_map = xor(cell_map, 1);
        
        %now look for white space
        trivial = true;
        t = (bg_idx == length(mu));
        white_map = reshape(t, m, m);
        [B, L] = bwboundaries( white_map );
        stats = regionprops(L, 'Area');
        for j=1:length(B)
            white_bounds = B{j};
            a = stats(j).Area;
            
            %only count areas as "white"  if there are big blocks of it
            if(a > 9500)
                trivial = false;
            end
        end        
        
        %use the maps to remove cells and white bg from raw image. leaving
        %only the texture (approx).
        raw_img = raw_img .* uint8(cell_map);
        
        %invert bits in white map
        if(~trivial)
            white_map = xor(white_map, 1);
            raw_img = raw_img .* uint8(white_map);
        end
        
        C = fdct_wrapping(raw_img, 0, 2, subbands);
        
        temp_data = [];
        
        %levels
        L = size(C, 2);
        for l=1:subbands
            a_mean = 0;
            a_std = 0;
            %angles
            A = size(C{l}, 2);
            for a=1:A
                coef = real(C{l}{a}(:));
                a_mean = a_mean + mean(coef);
                a_std = a_std + std(coef);
            end
            a_mean = a_mean / A;
            a_std = a_std / A;
            temp_data = [temp_data; [a_mean, a_std]];
        end
        
        disp( [temp_data(1, 2), temp_data(6, 2)] )
        sigma_data(:, i) = temp_data(:, 2);
        mu_data(:, i) = temp_data(:, 1);

        
        %{
        h = figure;
        set(h, 'Position', [0 0 screen_size(3) screen_size(4) ] );
        img = fdct_wrapping_dispcoef(C);
        subplot(1,2,1); colormap gray; imagesc(raw_img); axis('image'); 
        title('original image');
        subplot(1,2,2); colormap gray; imagesc(abs(img)); axis('image'); 
        title('log of curvelet coefficients');        
        pause;
        close all;
        %}
    end
    
    all_sigma_data(:, :, f) = sigma_data;
    all_mu_data(:, :, f) = mu_data;
    
end


%do some gaussian KDE
granularity = 2^12;

for g=1:size(all_sigma_data, 1)
 
    min(all_sigma_data(g, :, 1)) - range(all_sigma_data(g, :, 1))/4;
    max(all_sigma_data(g, :, 1)) + range(all_sigma_data(g, :, 1))/4;
    
    [h_c1, density_c1, mesh_c1, cdf_c1] = kde( ...
        all_sigma_data(g, :, 1), ...
        granularity, ...
        min(all_sigma_data(g, :, 1)) - range(all_sigma_data(g, :, 1))/4,...
        max(all_sigma_data(g, :, 1)) + range(all_sigma_data(g, :, 1))/4 ...
    );

    [h_c2, density_c2, mesh_c2, cdf_c2] = kde( ...
        all_sigma_data(g, :, 2), ...
        granularity, ...
        min(all_sigma_data(g, :, 2)) - range(all_sigma_data(g, :, 2))/4,...
        max(all_sigma_data(g, :, 2)) + range(all_sigma_data(g, :, 2))/4 ...
    );


    h = figure;
    set(h, 'Position', [0 0 640 480 ] );
    hold on; plot(mesh_c1, (density_c1 / sum(density_c1))');
    hold on; plot(mesh_c2, (density_c2 / sum(density_c2))', 'r');  

    hold on; scatter(all_sigma_data(g, :, 1), 0 * ones(1, 16))
    hold on; scatter(all_sigma_data(g, :, 2), 0 * ones(1, 16), 'r')
      
    foo = max( max(density_c1 / sum(density_c1)), max(density_c2 / sum(density_c2)));
    hold on; scatter(all_sigma_data(g, :, 3), 1.2*foo * ones(1, 16), 'g')
    hold on; scatter(all_sigma_data(g, :, 4), 1.1*foo * ones(1, 16), 'm')
    
    title(sprintf('subband %d curvelet coef distibution', g));
    legend({'G3 Density Estimate', 'G4 Density Estimate', '04-G3' '16-G4', '07-G3G4', '14-G4G3'}, 'Location', 'BestOutside')
    %{
    figure;
    ylim([-2, 2]);
    hold on; scatter(all_sigma_data(g, :, 1), -.5 * ones(1, 16))
    hold on; scatter(all_sigma_data(g, :, 2), .5 * ones(1, 16))
    hold on; scatter(all_sigma_data(g, :, 3), -1 * ones(1, 16))
    hold on; scatter(all_sigma_data(g, :, 4), 1 * ones(1, 16))
    %}
end


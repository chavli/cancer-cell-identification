fileids = {'04', '16'};
M = 512;   %desired image size 2Mx2M
m = 256;


for id=1:length(fileids)
    filename = sprintf('images/0471-Feu-%s-1.tif', fileids{id});
    img_data = imread(filename, 'TIFF');    
    
    %convert to gray-scale
    %gs_img_data = rgb2gray(img_data);
    img_dim = size(img_data, 1)/2;

    %cut out a square area of the image from the middle
    %midimg = img_data((img_dim-M:img_dim+(M-1)), (img_dim-M:img_dim+(M-1)), :);
    noisy_img = imnoise(img_data, 'poisson');
    figure; imshow(noisy_img);
    
    filename = sprintf('images/0471-Feu-%s-p-1.tif', fileids{id});
    imwrite(noisy_img, filename, 'TIFF');
end







addpath('./fdct_wrapping_matlab');
addpath('./fdct_usfft_matlab');
addpath('./ridgelet');
addpath('/home/cha/Programming/octave-libs/mlcl');
%close all;
clc; clear;


fileids = {'04', '16', '07', '14'};
data = [];


for f=1:1
    for i=1:9
        edgefile = sprintf('1-edge%d-0471-Feu-%s-1.tif', i, fileids{f});
        texturefile = sprintf('1-txur%d-0471-Feu-%s-1.tif', i, fileids{f});
        cellfile = sprintf('1-cell%d-0471-Feu-%s-1.tif', i, fileids{f});
            
        edgepath = sprintf('model_data/%s', edgefile);
        texturepath = sprintf('model_data/%s', texturefile);
        cellpath = sprintf('model_data/%s', cellfile);
        
        edge_img = imread(edgepath, 'TIFF');
        texture_img = imread(texturepath, 'TIFF');
        cell_img = imread(cellpath, 'TIFF');
        figure('Position', [100, 400, 1500, 500]);
        subplot(1, 3, 1);
        imshow(cell_img);

        theta = 0:179;
        [R, xp] = radon(cell_img, theta);
        %R = R ./ max(max(R));
        %R = R(:, [1:5, 87:92, 17:179]);
        subplot(1, 3, 2);
        imshow(R,[],'Xdata',theta,'Ydata',xp,'InitialMagnification','fit')
        xlabel('\theta (degrees)')
        ylabel('x''')
        colormap(hot), colorbar;

        %[d, pc, v, M] = pca_reduce(R, .75);
        [d, pc, v] = pca_reduce_d(R, 3);

        %data = [data; d];

        C = fdct_usfft(cell_img, 0);
        img = fdct_usfft_dispcoef(C);

        %figure; imshow(texture_img);
        colormap('jet');
        subplot(1, 3, 3); imagesc(abs(img));

    end
end

%{
figure;
scatter3(data(1:367, 1), data(1:367, 2), data(1:367, 3), 'o');
hold on;
scatter3(data(368:735, 1), data(368:735, 2), data(368:735, 3), 'v');
%}

%scatter3(data(735:27, 1), data(19:27, 2), data(19:27, 3), '*');
%scatter3(data(28:36, 1), data(28:36, 2), data(28:36, 3), '+');

function [ b_img ] = imgaddborder( img_data, width, color )
%IMGADDBORDER Summary of this function goes here
%   Detailed explanation goes here
    b_img = cat(3, img_data, img_data, img_data);    
    
    %top border
    b_img(1:width, :, 1) = b_img(1:width, :, 1) * color(1);
    b_img(1:width, :, 2) = b_img(1:width, :, 2) * color(2);
    b_img(1:width, :, 3) = b_img(1:width, :, 3) * color(3);
    
    %left border
    b_img(:, 1:width, 1) = b_img(:, 1:width, 1) * color(1);
    b_img(:, 1:width, 2) = b_img(:, 1:width, 2) * color(2);
    b_img(:, 1:width, 3) = b_img(:, 1:width, 3) * color(3);
    
    %bottom border
    b_img(end-width:end, :, 1) = b_img(end-width:end, :, 1) * color(1);
    b_img(end-width:end, :, 2) = b_img(end-width:end, :, 2) * color(2);
    b_img(end-width:end, :, 3) = b_img(end-width:end, :, 3) * color(3);
    
    %right border
    b_img(:, end-width:end, 1) = b_img(:, end-width:end, 1) * color(1);
    b_img(:, end-width:end, 2) = b_img(:, end-width:end, 2) * color(2);
    b_img(:, end-width:end, 3) = b_img(:, end-width:end, 3) * color(3);    
    
end


function [ gimg_out, idx_out ] = imgkmeans( gimg_in, m, c, mu)
%IMGKMEANS performs image segmentation on the given image using K-means
%clustering.
%
%   arguments:
%       gimg_in     -   the input gray-scale square image
%       m           -   side-length of square image
%       c           -   cluster size. for example c = 2 divides the mxm
%                       image into smaller cxc non-overlapping sections.
%                       the average "color" of each cluster is treated as a
%                       single datapoint in the kmeans process.
%       K           -   the number of means
%
%   returns
%       gimg_out    -   the segmented image
%
    
    if c > 1
        clusters = imgsectors(gimg_in, c, c, 1);
        avg_color = mean(mean(clusters));
        avg_color = avg_color(:);        
    else
        clusters = gimg_in(:);
        avg_color = clusters; 
    end
    
    %perform kmeans
    K = size(mu, 1);
    opts = statset('MaxIter', 200);
    idx = kmeans(avg_color, [], 'Start', mu, 'EmptyAction', 'drop', 'Options', opts);
    
    num_clus = m/c;
    
    if( length(idx) ~= num_clus^2 )
        idx_out = 0;
        gimg_out = gimg_in;
    else
        segged_img = reshape(idx, num_clus, num_clus);
    
        %rebuld the mxm image
        gimg_out = zeros(m, m);
        for i=1:num_clus
            for j=1:num_clus
                v = segged_img(i, j) ./ K; %scale it to 0-1
                if c > 1
                    gimg_out( ((c*j) - (c - 1)):(c*j),((c*i) - (c - 1)):(c*i)) = v;
                else
                    gimg_out( ((c*i) - (c - 1)):(c*i),((c*j) - (c - 1)):(c*j)) = v;
                end
            end
        end

        idx_out = gimg_out .* K;
    end
end


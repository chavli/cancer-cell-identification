%IMGSECTORS 
%   takes an image map and breaks into N square pieces with side length
%   resolution. e.g. if in_image is MxN, and m = M/2, n = N/2 in_image
%   will be broken into a 2x2 grid holding 4 pieces. if in_image is not 
%   square, the edge pieces will be truncated.
%   
%   arguments:
%       in_image    -   input image map
%       m           -   piece width 
%       n           -   piece height
%
%   returns:
%       img_grid    -   a mxnxDxI matrix, where each i in I is a mxn grid 
%                       of in_image (row major order)
%
function [ img_grid ] = imgsectors( in_image, m, n, scale)
    M = size(in_image, 1);
    N = size(in_image, 2);
    D = size(in_image, 3);
    
    %create the piece ranges
    row_e = (m:m:M);
    col_e = (n:n:N);


    %handle cases where in_image is not evenly divided by m and n
    if row_e(end) ~= M
        row_e = [row_e, M];
    end

    if col_e(end) ~= N
        col_e = [col_e, N];
    end

    row_s = row_e - (m-1);
    col_s = col_e - (n-1);

    %pre-allocate memory
    num_elements = ceil(M / m) * ceil(N / n);
    
    img_grid = zeros(m, n, D, num_elements);
    
    c = 1;
    for i=1:length(row_s)
        for j=1:length(col_s)
            img_grid(:, :, :, c) = in_image( (row_s(i):row_e(i)), ...
                                        (col_s(j):col_e(j)), ...    
                                        :);
            c = c + 1;
        end
    end

    img_grid = img_grid / scale;
end


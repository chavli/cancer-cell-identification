function [ labels ] = mahalkmeans( data_m, K )
%MAHALKMEANS Performs K-Means clustering using mahalanobis distance for
%gaussian-like data
%
%   The same idea as standard K-means except distance is calculated as:
%   
%   D_i = (x - x_i)' * sigma_i * (x - x_i);
%   
%   where i is a cluster 0 < i <= K

    M = size(data_m, 1);
    N = size(data_m, 2);
    CHUNK = 128;

    %initialize means to randomly chosen samples and set initial cov to 1
    idx = round(1 + (M - 1) * rand(K, 1));
    mu = data_m(idx); 
    sigma = zeros(N, N, K);
    for k=1:K
        sigma(:, :, k) = eye(N);
    end

    %100 iterations
    labels = zeros(M, 1);
    D = ones(M, 1) * Inf;
    for i=1:100
        %cluster the samples
        for k=1:K
            V = bsxfun(@minus, data_m, mu(k, :));
            
            %break the calculation into chunks
            pieces = 1:CHUNK:(M+(CHUNK-1));

            for c=1:(length(pieces)-1)
                range = pieces(c):(pieces(c+1)-1);
                v = V(range);
                d = v * sigma(:, :, k) * v';
                
                %the distances are along the diagonal of d
                dists = diag(d);
                
                %now some crazy bsxfun to update the cluster the points are
                %in
                mins = bsxfun(@lt,  dists, D(range));
                idx = find(mins == 1);

                labels(range(idx)) = k;
                D(range(idx)) = dists(idx);
            end
            
        end
        
        %update the parameters
        for k=1:K
            idx = find(labels == k);
            group = data_m(idx, :);

            mu(k, :) = mean(group);
            sigma(:, :, k) = cov(group);
        end           
    end
end

function [ idx ] = meanshift( data_m )
%MEANSHIFT Implementation of the mean shift algorithm using a gaussian
%kernel.
%
%   Mean Shift is a non-parametric mean seeking algorithm used for
%   clustering data points. 
%
%   The algorithm is first described by Fukunaga and Hostetler:
%   "The Estimation of the Gradient of a Density Function, with 
%   Applications in Pattern Recognition"
%
%   arguments:
%       data_m  -   the sample data to cluster. rows are samples columns
%                   are variables.
%       K       -   the number of means to estimate
%
    M = size(data_m, 1);
    N = size(data_m, 2);
    
    h = (1.06 * std(data_m)) / (M^(.2));
    
    %max iterations set to 100
    for i=1:1
        K = data_m;

        for m=1:M
            D = zeros(M, 1);
            for n=1:M
                v = data_m(n, :) - K(m, :);
                D(n, :) = exp(-((norm(v ,2)/h)^2));
            end
            K(m, :) = sum(D' * data_m) / sum(D);
        end
    
    end
    
    figure; hist(K);
        
end


\documentclass[12pt]{article}

\usepackage[fleqn]{amsmath}

\usepackage{amsfonts}

\usepackage{graphicx}

\usepackage{algorithm}

\usepackage{algorithmic}

\usepackage{amsthm}

\usepackage{caption}

\usepackage{subcaption}

\usepackage[hmargin=1cm,vmargin=1cm]{geometry}
\graphicspath{{./images/}}
%\usepackage{graphicx} %allows use of figures

%\pagestyle{empty} %<== DELETE IF YOUR TEST HAS MORE THAN ONE PAGE

% (this page style suppresses page numbers.)

%Adjust the margins:


%puts in a horizontal rule the width of the text with space

%before and after.

\newcommand{\sep}{\ifhmode\par\fi\vskip6pt\hrule\vskip6pt}

\newcounter{rcounter}

%A new environment for a test problem.

\newcounter{problem}

\newenvironment{problem}

{

\stepcounter{problem}%

\filbreak%

\sep

\noindent\arabic{problem}.

}

{

\filbreak

}



\begin{document}
{
\raggedright
\textsc{Cha Li\hfill \footnotesize {CS3790: Pattern Recognition}}\\
\textsc{\footnotesize{chavli@cs.pitt.edu} \\
\vspace{.05in}
\footnotesize {
Term Project: Gleason Classification\\
22 April 2013
}}}
\vspace{.1in}
\pagestyle{empty} %remove page numbers
\hrule
\vspace{.1in}
%
% start typing from here
%
\paragraph{Title} Machine Differentiation of Grade 3 and Grade 4 Prostate Cancer

\paragraph{Introduction}
The Gleason grading system is used to determine the developmental stage of prostate cancer in men. Grading is performed on tissue
samples from patients and used for prognosis and possible treatments. A Gleason score ranges between 2 and
10 with higher scores being worse. Furthermore, the score is broken down into a primary grade and secondary grade. The primary 
grade represents the cancer pattern in the majority of cells ($> 50\%$) and the secondary grade represents the cancer pattern in the
minority of cells ($< 50\%$ and $> 5\%$). Grades range from 1 to 5. 


\paragraph{Motivation}
Gleason grading is typically done by doctors on a case by case basis. Since this process involves
humans, grading is prone to several problems. First, the task is extremely time-consuming and tedious as tissue samples
must be examined in great detail. Second, when faced with many tissue samples a human grader may become fatigued and provide
inconsistent and erroneous scores. Third, it is impractical to expect humans to be the primary judge as medical data becomes 
increasingly digitized and stored in large quantities. These problems are made worse by the difficulty and importance of differentiating
between grades 3 and 4, which define the line between benign and malignant cells. Given these problems, Gleason grading is an ideal
domain for image processing and machine learning. 

\paragraph{Project Overview}
In this report I discuss a method for processing prostate cancer images and classifying them into their proper Gleason grades. The images
used in this project were provided by the Johns Hopkins Medical Institute with permission from Dr. R. Veltri. Four images were provided 
with the following Gleason scores: G3/G3, G4/G4, G3/G4, and G4/G3 (primary/secondary). The next few sections will discuss, 
images processing, feature extraction, and SVM classification. All code is written in MATLAB.

\paragraph{Preprocessing}
The raw images provided by Dr. Veltri are high-resolution (1650x1650) dyed (blue) tissue samples (Figure~\ref{fig:original}). Each raw 
image is converted to gray-scale and then reduced to its center 1024x1024 pixels to remove the surrounding white area. Sixteen 256x256
sub-images are then cut from each reduced image, providing a total of 64 images. Figure~\ref{fig:section256} will be used
as the example image to for this report.

\begin{figure}[ht!]
\centering
\begin{subfigure}[ht]{0.3\textwidth}
\centering
\includegraphics[width=2in]{0471-Feu-07-1}
\caption{}
\label{fig:original}
\end{subfigure}%
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.3\textwidth}
\centering
\includegraphics[width=2in]{unprocessed}
\caption{ }
\label{fig:unprocessed}
\end{subfigure}
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.3\textwidth}
\centering
\includegraphics[width=2in]{example256}
\caption{}
\label{fig:section256}
\end{subfigure}
\caption{(\subref{fig:original}) A raw 1650x1650 blue dyed image. (\subref{fig:unprocessed}) The center 1024x1024 pixels of a 
gray-scaled Figure~\ref{fig:original}. (\subref{fig:section256}) An example of one of the 256x256 sub-images used for this 
work. This specific image will be used for examples throughout this report.}
\label{fig:preprocess}
\end{figure}

\paragraph{Image Processing}
For the task of Gleason grading, we want to be able to describe the contents of each image. Specifically, we would like to be able to
separate an image into three categories: cells, texture, and background. Isolation allows each category to examined without interference
from the other categories. For example, to examine the texture, the background and cells should be removed. This segmentation is
achieved by using image sharpening and background blurring techniques combined with basic k-means clustering. Contrast sharpening 
tries to make the distribution of pixel intensities more Gaussian, a skewed distribution results in an overly dark or light image. This 
operation can be done in MATLAB using the \texttt{adapthisteq} and \texttt{boxcox} functions, the results are shown in Figure
~\ref{fig:sharpened}. Additionally, anisotropic diffusion (AD) is applied to the sharpened to create a blurred image, an example is shown in 
Figure~\ref{fig:blurred}. AD blurs the background noise while maintaining edge defining properties~\cite{perona1990}.

The pixel intensities of the gray-scale images are real values that range from 0 to 1. $k$-means clustering is used to segment the images
into cells, texture, and background using initial mean values of $\boldsymbol{\mu} = \{0, .25, .5, .75\}$ $(k = 4)$. Clustering results
can then be used to create binary images that approximate cells and the background. 

Edge tracing is applied to the binary images using the \texttt{bwboundaries} MATLAB function which also extracts properties of the
traced regions. \texttt{bwboundaries} implements the Moore-Neighbor tracing algorithm. The results of edge tracing are shown in
Figure~\ref{fig:edge-process}, where the edges are drawn over the original sub-images.
\begin{figure}[ht!]
\centering
\begin{subfigure}[ht]{0.29\textwidth}
\centering
\includegraphics[width=\textwidth]{sharpened}
\caption{}
\label{fig:sharpened}
\end{subfigure}%
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.29\textwidth}
\centering
\includegraphics[width=\textwidth]{kmeans_sharp}
\caption{}
\label{fig:kmeans-sharp}
\end{subfigure}
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.29\textwidth}
\centering
\includegraphics[width=\textwidth]{bw_sharp}
\caption{}
\label{fig:bw-sharp}
\end{subfigure}
\caption{ (\subref{fig:sharpened}) The sharpened example image, dark cells stand out much more. (\subref{fig:kmeans-sharp})
k-means separates the image into segments where the gray areas represent cells. (\subref{fig:bw-sharp}) The binary cell image. }
\label{fig:sharp-process}
\end{figure}


\begin{figure}[ht!]
\centering
\begin{subfigure}[ht]{0.29\textwidth}
\centering
\includegraphics[width=\textwidth]{blurred}
\caption{}
\label{fig:blurred}
\end{subfigure}%
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.29\textwidth}
\centering
\includegraphics[width=\textwidth]{kmeans_blur}
\caption{}
\label{fig:kmeans-blur}
\end{subfigure}
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.29\textwidth}
\centering
\includegraphics[width=\textwidth]{bw_blur}
\caption{ }
\label{fig:bw-blur}
\end{subfigure}
\caption{(\subref{fig:blurred}) The sharpened image with AD applied. Cells are about the same, but the background
is cleaner. (\subref{fig:kmeans-blur}) Reduced noise results in smoother, less precise, k-means defined regions. 
(\subref{fig:bw-blur}) Binary background image.}
\label{fig:blur-process}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}[ht]{0.3\textwidth}
\centering
\includegraphics[width=\textwidth]{celledge}
\caption{}
\label{fig:celledge}
\end{subfigure}%
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.3\textwidth}
\centering
\includegraphics[width=\textwidth]{whiteedge}
\caption{ }
\label{fig:whiteedge}
\end{subfigure}
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.3\textwidth}
\centering
\includegraphics[width=\textwidth]{trivialwhiteedge}
\caption{ }
\label{fig:trivialwhiteedge}
\end{subfigure}
\caption{(\subref{fig:celledge}) The detected cells of the example image are outlined in red. (\subref{fig:whiteedge}) Background
detected in our example image. The green outline represents a significant background region. In this work, when a large 
background region is detected, all background regions are removed (green and blue outlines). (\subref{fig:trivialwhiteedge}) 
An example of a sub-image without a significant background region.} 
\label{fig:edge-process}
\end{figure}



\paragraph{Cell Feature Extraction} A cell segmented image allows us to calculate many features describing cell properties. Ali et. al.
~\cite{ali2011adaptive} applies shape priors to detect overlapping cells which are then used to extract 7 morphological features. In this
work, overlapping cells are not considered and simpler features are extracted: number of cells, average cell roundness, variance of 
cell roundness, and spread of cells. A cell is defined as one outlined region (Figure~\ref{fig:celledge}). Additionally, the cell-area to 
sub-image-area ratio and the sub-image-texture-area to total-image-area ratio can be calculated. 

\paragraph{Texture Feature Extraction}
The curvelet transform is used to characterize the texture of each image. Combining the results shown in Figures~\ref{fig:celledge} and 
~\ref{fig:whiteedge} just the texture can be extracted from the example image as shown in Figure~\ref{fig:curvelets} (left). Curvelets 
are rotation invariant and can be used to detect edges and curves in an image~\cite{candes2006fast}. The output of the curvelet 
transform is a set of curvelet coefficients representing rotations at different sub-band levels. Figure~\ref{fig:curvelets} (right) shows 
the sub-bands(rings) of the example image. Coefficients are represented by pixel intensities. Gomez and Romero
~\cite{Gomez:2011:RIT:2064100.2064196} use a Generalized Gaussian distribution to characterize the distribution of curvelet coefficients
at each sub-band. In this work, the traditional Gaussian distribution is used, therefore, each sub-band is characterized by its empirical
mean and standard deviation.

\begin{figure}[h!]
\centering
\includegraphics[width=.75\textwidth]{curvelet}
\caption{(left) The texture only image of the example image. (right) The curvelet coefficients of the example image for different 
rotations of each sub-band.}
\label{fig:curvelets}
\end{figure}

Six sub-bands were calculated with the curvelet transform using the default number of rotations per sub-band. After examining the 
resulting Gaussian distributions, the calculated mean values were unable to differentiate the G3 and G4 Gleason grades, however, the
standard deviations were able to make this distinction. The distribution of standard deviation values of all 64 sub-images for three of 
six (to save space) sub-bands is shown in Figure~\ref{fig:subbands}. The example sub-image used so far for illustration is one of the 
green data points in each of the plots. In each sub-figure of Figure~\ref{fig:subbands}, the blue points represent the distribution for the 
G3/G3 sub-images, red points represent the distributions of the G4/G4 sub-images, green points for the G3/G4 sub-images, and pink 
points for the G4/G3 sub-images (blue and red respectively). The two curves represent the partial Gaussian kernel density estimations 
(KDE) of standard deviation for the ``pure" G3 and G4 images. In Figures~\ref{fig:subband2} and~\ref{fig:subband4} (sub-bands 2 and
4 respectively) the ``non-pure" images (green and pink data) hover over the same area of the density estimates and therefore are not 
differentiated, however, Figure~\ref{fig:subband6} (sub-band 6) shows a wider spread of the ``non-pure" data across the density 
estimates. 

\begin{figure}[ht!]
\centering
\begin{subfigure}[ht]{0.32\textwidth}
\centering
\includegraphics[width=\textwidth]{subband2}
\caption{}
\label{fig:subband2}
\end{subfigure}%
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.32\textwidth}
\centering
\includegraphics[width=\textwidth]{subband4}
\caption{}
\label{fig:subband4}
\end{subfigure}
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.32\textwidth}
\centering
\includegraphics[width=\textwidth]{subband6}
\caption{ }
\label{fig:subband6}
\end{subfigure}
\caption{These graphs represent all 64 sub-images. (\subref{fig:subband2}) KDE of distribution of std. dev. values for sub-band 2. 
G3/G4 (green) and G4/G3 (pink) data cluster around the G3 density and are not differentiable. (\subref{fig:subband4})KDE of 
distribution of std. dev. values for sub-band 4. G3/G4 (green) and G4/G3 (pink) data cluster around the G4 density and are not 
differentiable. (\subref{fig:subband6}) KDE of distribution of std. dev. values for sub-band 6. G3/G4 (green) and G4/G3 (pink) data 
have a nice spread over the G3 and G4 densities. }
\label{fig:subbands}
\end{figure}

\paragraph{Classification Setup}
With all the images properly characterized using their cell and texture information, each image is run through a support vector 
machine (SVM) for classification. We assume that the sub-images extracted from the ``pure" G3/G3 and G4/G4 images belong to their
respective grades. For the G3/G4 and G4/G3 images, we only know the overall image has a primary and secondary grade, the individual
grades of each sub-image are unknown. We formulate the learning problem as follows:
\begin{description}
\item[Model] Two-class SVM, G3 as class $-1$ and G4 as class $1$.
\item[Data] 31 training samples provided by the G3/G3 (15) and G4/G4 (16) images. 32 test samples provided by the G3/G4 (16) and 
G4/G3 (16) images.
\item[Features] Three features per sample: average cell roundness, spread of cells, and sub-band 6 distribution
\item[Weights] Two weight values used to augment the predicted class labels: cell-area to sub-image-area ratio and sub-image-area to
whole-image-area ratio.
\end{description}

Leave-one-out cross-validation using the training data results in an accuracy of $93.55\%$. All 16 sections of the G4/G4 image are 
classified correctly whereas only 13 sections of the G3/G3 image are classified correctly. The weighted results for each training image 
respectively are: 100\% G4, 0\% G3 and 86.21\% G3, 13.79\% G4.

\paragraph{Classification Results}
The following results are based on calibrating the SVM model parameters to the cross-validation results. Table~\ref{tbl:results} 
summarizes the results for each test image and Figure~\ref{fig:results} illustrates how each sub-image for each test image was 
classified. Sections with a blue outline are classified as G3 and sections with a green outline are classified as G4. Each section's contribution
to the classification of the whole image is weighted by how much data it contributes, determined by the ratios described earlier. The
results for both test images agree with human classification and with the definition of primary and secondary Gleason grades stated 
in the introduction.

\begin{table}[ht!]
\centering
\begin{tabular}{|c | c | c |}
\hline
& \textbf{G3/G4 Image} (0471-Feu-07-1) & \textbf{G4/G3 Image} (0471-Feu-14-1) \\
\hline
\textbf{Human Grading} & Primary: G3& Primary: G4\\
& Secondary: G4 & Secondary: G3\\
\hline
\hline
\textbf{Machine Grading} & &\\
&&\\
Individual Sections & G3: 9 Sections, G4: 7 Sections & G3: 6 Sections, G4: 10 Sections\\
&&\\
Overall Weighted Grade& Primary: G3 (52.59\%) & Primary: G4 (61.26\%)\\
& Secondary: G4 (47.41\%) & Secondary: G3 (38.74\%)\\
\hline
\end{tabular}
\caption{Classification results for the G3/G4 and G4/G3 images. The SVM output agrees with the human grades and with the definition
of primary and secondary grades.}
\label{tbl:results}
\end{table}

\begin{figure}[ht!]
\centering
\begin{subfigure}[ht]{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gleason-07}
\caption{}
\label{fig:gleason-07}
\end{subfigure}%
~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
%(or a blank line to force the subfigure onto a new line)
\begin{subfigure}[ht]{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Gleason-14}
\caption{}
\label{fig:gleason-14}
\end{subfigure}
\caption{ Sections outlined in blue were given a G3 classification and sections outlined in green were given a G4 classification. The
contribution of each section's classification to the overall image classification is weighted by how much information it provides.
(\subref{fig:gleason-07}) The reconstructed G3/G4 image from its 16 classified sub-images. (\subref{fig:gleason-14})
The reconstructed G4/G3 image from its 16 classified sub-images.}
\label{fig:results}
\end{figure}



\nocite{*}
\bibliographystyle{amsplain}
\bibliography{references}

\end{document}
